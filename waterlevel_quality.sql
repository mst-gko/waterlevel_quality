-- er pejling målt i kote?
SELECT guid 
FROM jupiter.watlevel wl
WHERE watlevmsl IS NOT NULL 

-- er pejling angivet som del i en synkronpejling
SELECT guid 
FROM jupiter.watlevel wl
WHERE watlevelroundno IS NOT NULL 

-- er pejling en del at tætliggende pejlinger fra tilnærmelsesvis samme tidspunkt, altså synkronpejling som ikke er angivet? 

-- er pejling fra en logger

-- er pejling angivet som rovandspejling
SELECT guid 
FROM jupiter.watlevel wl
WHERE situation = 0

-- er pejling en outlier ift. interquartile range?
WITH 
watlevel_filter AS 
	(
		SELECT 
			guid,
			intakeid,
			watlevmsl
		FROM jupiter.watlevel wl
		WHERE watlevmsl IS NOT NULL 
	),
quartiles AS 
	(
	 	SELECT 
		  	intakeid,
		    percentile_cont(0.25) WITHIN GROUP (ORDER BY watlevmsl) AS q1,
		    percentile_cont(0.75) WITHIN GROUP (ORDER BY watlevmsl) AS q3
	  	FROM watlevel_filter
	  	GROUP BY intakeid
	),
iqr_values AS 
	(
		SELECT
			intakeid,
			q1,
			q3,
			q3 - q1 AS iqr,
			q1 - 1.5 * (q3 - q1) AS iqr_lower,
			q3 + 1.5 * (q3 - q1) AS iqr_upper
	  	FROM quartiles
	)
SELECT 
	t.guid
FROM watlevel_filter t
JOIN iqr_values i ON t.intakeid = i.intakeid
WHERE t.watlevmsl >= iqr_lower
	AND t.watlevmsl <= iqr_upper
;

-- er pejling en outlier i indtaget ift. z-score
WITH 
watlevel_filter AS 
	(
		SELECT 
			guid,
			intakeid,
			watlevmsl
		FROM jupiter.watlevel wl
		WHERE watlevmsl IS NOT NULL 
	),
stats AS 
	(
		SELECT 
		    intakeid,
		    AVG(watlevmsl) AS watlevmsl_avg,
		    STDDEV(watlevmsl) AS watlevmsl_std
		FROM watlevel_filter
		GROUP BY intakeid
	),
z_scores AS 
	(
		SELECT 
		    wl.guid,
		    (wl.watlevmsl - stats.watlevmsl_avg) / stats.watlevmsl_std AS z_score
		FROM watlevel_filter wl
		INNER JOIN stats ON wl.intakeid = stats.intakeid
	)
SELECT 
	guid
FROM z_scores
WHERE z_scores.z_score <= 3
;

-- er pejling et positivt tal?
SELECT guid 
FROM jupiter.watlevel wl
WHERE watlevmsl > 0 

-- er pejling en outlier ift. at være indenfor 1 standard afvigelse?
WITH 
watlevel_filter AS 
	(
		SELECT 
			guid,
			intakeid,
			watlevmsl
		FROM jupiter.watlevel wl
		WHERE watlevmsl IS NOT NULL 
	),
stats AS 
	(
	    SELECT 
	        intakeid,
	        AVG(watlevmsl) AS mean_value,
	        STDDEV(watlevmsl) AS stddev_value
	    FROM watlevel_filter
	    GROUP BY intakeid
	)
SELECT 
    t.watlevmsl,
    s.mean_value,
    s.stddev_value
FROM watlevel_filter t
INNER JOIN stats s ON t.intakeid = s.intakeid
WHERE t.watlevmsl < s.mean_value - s.stddev_value 
	OR t.watlevmsl > s.mean_value + s.stddev_value
;

-- er kvalitet allerede angivet af geus som god og uden anmærkninger
SELECT guid
FROM jupiter.watlevel wl
WHERE wl.quality = 'G' 
	AND wl.qualitycontrol IS NULL 


/*
SELECT  
	bh.boreholeid,
	wl.intakeno, 
	bh.locatmetho,
	bh.elevametho,
	wl.situation, 
	s.top,
	s.bottom,
	wl.guid AS guid_watlevel,
	wl.timeofmeas, 
	wl.waterlevel,
	wl.watlevgrsu,
	wl.watlevmsl,
	wl.watlevmp,
	wl.quality,
	wl.qualitycontrol,
	wl.refpoint 
FROM jupiter.watlevel wl
LEFT JOIN jupiter.borehole bh ON wl.boreholeid = bh.boreholeid  
--LEFT JOIN jupiter.intake i ON wl.intakeid = i.intakeid
LEFT JOIN jupiter.screen s ON s.intakeid = wl.intakeid
	*/